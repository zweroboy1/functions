const tickets = (people) => {
  people = people.map(el => Number(el));
  let count25 = 0;
  let count50 = 0;
  for (let money of people){
    if(money === 25){
      count25++;
    }
    if(money === 50){
      count25--;
      count50++;
    }
    if(money === 100){
      if(count50 == 0 && count25 >= 3){
        count25 -= 3;
      } else{
        count25--;
        count50--;
      }
    }
    if(count25 < 0 || count50 < 0){
       return 'NO';
    }
  }
  return 'YES';
}

const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;    
  }
  if (/[^\d]/.test(str1) || /[^\d]/.test(str2)){
    return false;    
  }
  if (str1 === '') {
    str1 = '0';
  }
  if (str2 === '') {
    str2 = '0';
  }
  let result = '';
  let prev = 0;
  for (let i = Math.max(str1.length, str2.length); i--; i >= 0) {
    let num1 = Number(str1[i]) || 0;
    let num2 = Number(str2[i]) || 0;
    let sum = prev + num1 + num2;
    if (sum > 9) {
      sum-=10;
      prev = 1;
    } else {
      prev = 0;
    }
    result = sum  + result;
  }
  if (prev !== 0) {
    result = prev + result;
  }
  return result;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let commentCount = 0;
  listOfPosts.forEach(({author, comments}) => {
    if (author === authorName) {
      postCount++;
    }
    if (comments !== undefined) {
      commentCount += comments.filter(el => el.author === authorName).length;
    }
  });
  return `Post:${postCount},comments:${commentCount}`;
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
